const express = require('express')
const app = express()
const port = 3000
const URLBase = '/apiperu'
const URLBaseMlab = '/apiperu/mlab'
const version1 = '/v1'
const version2 = '/v2'
const version3 = '/v3'

var userFile = require('./users.json')
var bodyParser = require('body-parser')
var dateFormat = require('dateformat')
var requestJson = require('request-json')


app.use(bodyParser.json())
app.listen(port)

app.get(URLBase+version1+'/users', function(req, res){
  console.log(`GET ${URLBase}${version1}/users`);
  res.status(200);
  res.send(userFile);
});

app.get(URLBase+version2+'/users', function(req, res){
  console.log(`GET ${URLBase}${version2}/users`);
  res.status(200);
  res.send(req.query);
});

app.get(URLBase+version3+'/users/:user_id/phones/:phone_id', function(req, res){
  console.log(`GET ${URLBase}${version1}/users/`+req.params.user_id+"/phones/"+req.params.phone_id);
  res.status(200);
  res.send({"query":req.query,"params":req.params});
});

app.get(URLBase+version1+'/users/:user_id', function(req, res){
  console.log(`GET ${URLBase}${version1}/users/`+req.params.user_id);
  let user_id = parseInt(req.params.user_id);
  let user = findUserById(parseInt(req.params.user_id));

  if(user != undefined){
    res.status(200);
    res.send(user);
  }else{
    res.send({"msg":"User dosn't exist."});
  }
});

app.post(URLBase+version1+'/users', function(req, res){
  console.log(`POST ${URLBase}/users`);

  var now = new Date();
  //let newId = userFile.length+1;
  let newId = +dateFormat(now, "yyyymmddhMMss");
  let newUser = {
    "user_id":newId,
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":req.body.password
  }
  userFile.push(newUser);
  res.status(201);
  res.send({"msg":"User created.","user":newUser});
});

app.put(URLBase+version1+'/users/:user_id', function(req, res){
  console.log(`PUT ${URLBase}${version1}/users/`+req.params.user_id);

  let user = findUserById(parseInt(req.params.user_id));

  if(user != undefined){
    user.first_name = req.body.first_name;
    user.last_name = req.body.last_name;
    user.email = req.body.email;
    user.password = req.body.password;

    res.status(200);
    console.log(userFile);
    res.send({"msg":"User "+user.user_id+" modified","user":user});
  }else{
    res.status(400);
    res.send({"msg":"User doesn't exist."});
  }
});

app.delete(URLBase+version1+'/users/:user_id', function(req, res){
  console.log(`DELETE ${URLBase}${version1}/users/`+req.params.user_id);

  var pos = -1;
  var user_id = parseInt(req.params.user_id)
  for(var i = 0; i < userFile.length; i++){
    if(userFile[i].user_id == user_id){
      pos = i;
      break;
    }
  }
  if(pos!=-1){
    userFile.splice(pos,1);
    res.status(200);
    res.send({"msg":"User "+user_id+" deleted"});

  }else{
    res.status(400);
    res.send({"msg":"User doesn't exist."});
  }
});

function findUserById(user_id){
  for(var i = 0; i < userFile.length; i++){
    if(userFile[i].user_id == user_id){
      user = userFile[i];
      return user;
    }
  }
}

const MLAB_API_KEY = 'apiKey=f-0MojUgsOTWJS-JYabVXWtktKrRopRm';
const URI_BD_MLAB = 'https://api.mlab.com/api/1';

var mLabClient = requestJson.createClient(URI_BD_MLAB);

app.get(URLBaseMlab+version1+'/users', function(req, res){
  console.log(`GET ${URLBaseMlab}${version1}/users`);

  let query = "f={'_id':0}";

  mLabClient.get(URI_BD_MLAB+'/databases/apiperudb/collections/user?'+query+"&"+MLAB_API_KEY, function(err, resM, body) {
    if(!err){
      if(body.length>0){
        res.status(200);
        res.send(body);
      }else{
        res.status(204);
      }
    }else{
      res.status(500)
    }
  });
});

app.get(URLBaseMlab+version1+'/users/:user_id', function(req, res){
  console.log(`GET ${URLBaseMlab}${version1}/users/`+req.params.user_id);

  let query = "q={'user_id':"+req.params.user_id+"}&f={'_id':0}";

  mLabClient.get(URI_BD_MLAB+'/databases/apiperudb/collections/user?'+query+"&"+MLAB_API_KEY, function(err, resM, body) {
    if(!err){
      if(body.length>0){
        res.status(200);
        res.send(body);
      }else{
        res.status(204);
        res.send({"msg":"User doesn't exist."})
      }
    }else{
      res.status(500)
      res.send({"msg":"Service unavailable."})
    }
  });
});

app.put(URLBaseMlab+version1+"/users/:user_id", function(req, res){
  console.log(`PUT ${URLBaseMlab}${version1}/users/`+req.params.user_id);

  let query = "q={'user_id':"+req.params.user_id+"}";
  let cambio = '{"$set":'+JSON.stringify(req.body)+'}'

  mLabClient.get(URI_BD_MLAB+'/databases/apiperudb/collections/user?'+query+"&"+MLAB_API_KEY, function(errG, resG, bodyG){
    if(!errG){
      if(bodyG.length>0){
        mLabClient.put(URI_BD_MLAB+'/databases/apiperudb/collections/user/'+bodyG[0]._id.$oid+"?"+MLAB_API_KEY, JSON.parse(cambio), function(errP, resP, bodyP){
          if(!errP){
            res.status(200);
            res.send({"msg":"Updated","user":bodyP})
          }else{
            res.status(500)
            res.send({"msg":"Service unavailable."})
          }
        });
      }else{
        res.status(204);
        res.send({"msg":"User doesn't exist."})
      }
    }else{
      res.status(500)
      res.send({"msg":"Service unavailable."})
    }
  });

});


app.delete(URLBaseMlab+version1+"/users/:user_id", function(req, res){
  console.log(`DELETE ${URLBaseMlab}${version1}/users/`+req.params.user_id);

  let query = "q={'user_id':"+req.params.user_id+"}";

  mLabClient.get(URI_BD_MLAB+'/databases/apiperudb/collections/user?'+query+"&"+MLAB_API_KEY, function(errG, resG, bodyG){
    if(!errG){
      if(bodyG.length>0){
        mLabClient.delete(URI_BD_MLAB+'/databases/apiperudb/collections/user/'+bodyG[0]._id.$oid+"?"+MLAB_API_KEY, function(errD, resD, bodyD){
          if(!errD){
            res.status(200);
            res.send({"msg":"Deleted","user":bodyD})
          }else{
            res.status(500)
            res.send({"msg":"Service unavailable."})
          }
        });
      }else{
        res.status(204);
        res.send({"msg":"User doesn't exist."})
      }
    }else{
      res.status(500)
      res.send({"msg":"Service unavailable."})
    }
  });

});



app.post(URLBaseMlab+version1+"/login", function(req, res){
  console.log(`POST ${URLBaseMlab}${version1}/login`);

  let query = "q={'user_id':"+req.params.user_id+"}";

  mLabClient.get(URI_BD_MLAB+'/databases/apiperudb/collections/user?'+query+"&"+MLAB_API_KEY, function(errG, resG, bodyG){
    if(!errG){
      if(bodyG.length>0){
        mLabClient.delete(URI_BD_MLAB+'/databases/apiperudb/collections/user/'+bodyG[0]._id.$oid+"?"+MLAB_API_KEY, function(errD, resD, bodyD){
          if(!errD){
            res.status(200);
            res.send({"msg":"Deleted","user":bodyD})
          }else{
            res.status(500)
            res.send({"msg":"Service unavailable."})
          }
        });
      }else{
        res.status(204);
        res.send({"msg":"User doesn't exist."})
      }
    }else{
      res.status(500)
      res.send({"msg":"Service unavailable."})
    }
  });

});
